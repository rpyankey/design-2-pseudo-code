package Applicances;

import java.util.Date;

public class Curtain implements ElectronicAppliance{
    private boolean switchOn;
    private Date alarmtime;

    public Curtain(boolean switchOn, Date alarmtime) {
        this.switchOn = switchOn;
        this.alarmtime = alarmtime;
    }

    public boolean isSwitchOn() {
        return switchOn;
    }

    public void setSwitchOn(boolean switchOn) {
        this.switchOn = switchOn;
    }

    public Date getAlarmtime() {
        return alarmtime;
    }

    public void setAlarmtime(Date alarmtime) {
        this.alarmtime = alarmtime;
    }
}
