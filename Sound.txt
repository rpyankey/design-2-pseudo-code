package Applicances;

public class Television {
    private boolean switchOn;
    private double timeLimit;
    private String content;

    public Television(boolean switchOn, String content, double timeLimit) {
        this.switchOn = switchOn;
        this.content = content;
        this.timeLimit = timeLimit;
    }

    public boolean isSwitchOn() {
        return switchOn;
    }

    public void setSwitchOn(boolean switchOn) {
        this.switchOn = switchOn;
    }

    public double getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(double timeLimit) {
        this.timeLimit = timeLimit;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
